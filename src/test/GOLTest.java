package test;
import static org.junit.Assert.*;

import java.util.Random;

import org.junit.Test;

import GameOfLife.GameOfLife;

public class GOLTest {

	public static final String newline = System.getProperty("line.separator");
	//Test states not evolving (dead or static "still life") 
	public static final String staticStateSquare = "0 0 0 0 "+newline+"0 1 1 0 "+newline+"0 1 1 0 "+newline+"0 0 0 0 "+newline;
	public static final String staticStateCorner = "1 0 0 1 "+newline+"0 0 0 0 "+newline+"0 0 0 0 "+newline+"1 0 0 1 "+newline;
	public static final String staticStateDead = "0 0 0 0 "+newline+"0 0 0 0 "+newline+"0 0 0 0 "+newline+"0 0 0 0 "+newline;
	public static final String staticStateBad = "0 0 0 0 "+newline+"0 0 0 "+newline+"0 "+newline+"0 0 "+newline;
	//evolving states periodic or non-periodic
	public static final String periodicStateBeltHorizontal[] =
		{"0 0 0 0 "+newline+"1 1 1 1 "+newline+"1 1 1 1 "+newline+"0 0 0 0 "+newline,
				"1 1 1 1 "+newline+"0 0 0 0 "+newline+"0 0 0 0 "+newline+"1 1 1 1 "+newline};
	public static final String oscillatoryStateBeltVertical[] =
		{"0 1 1 0 "+newline+"0 1 1 0 "+newline+"0 1 1 0 "+newline+"0 1 1 0 "+newline,
				"1 0 0 1 "+newline+"1 0 0 1 "+newline+"1 0 0 1 "+newline+"1 0 0 1 "+newline};
	public static final String unstableStateString[] =
		{"0 0 0 0 "+newline+"1 1 1 1 "+newline+"0 0 0 0 "+newline+"0 0 0 0 "+newline,
				"1 1 1 1 "+newline+"1 1 1 1 "+newline+"1 1 1 1 "+newline+"0 0 0 0 "+newline,
				staticStateDead};
	
	@Test
	public void shouldCreateDeadCellGrid() {
		//given
		GameOfLife gol = new GameOfLife(4);
		//when
		boolean living = gol.anyLiving();
		//then
		assertEquals(false,living);
	}
	
	@Test
	public void shouldNotCreateGridSizeBelowFour() {
		//given
		boolean exceptionCaught= false;
		try {
			GameOfLife gol = new GameOfLife(3);
		}
		catch (Exception e){
			//when
			exceptionCaught=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void shouldPrintEmptyGrid() {
		//given
		GameOfLife gol = new GameOfLife(4);
		//when
		String grid = gol.toString();
		boolean result=grid.equals(staticStateDead);
		//then
		assertEquals(true,result);
	}
	
	@Test
	public void shouldFlipOneElement() {
		//given
		GameOfLife gol = new GameOfLife(4);
		Random rand = new Random();
		int randomRow = rand.nextInt(4);
		int randomColumn= rand.nextInt(4);
		gol.flip(randomRow,randomColumn);
		//when
		boolean result=gol.isAlive(randomRow, randomColumn);
		//then
		assertEquals(true,result);
	}

	@Test
	public void shouldNotFlipOutOfBonds() {
		//given
		GameOfLife gol = new GameOfLife(4);
		boolean exceptionCaught=false;
		//when
		try {
			gol.flip(-1,2);
		}
		catch (Exception e) {
			exceptionCaught=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void shouldCreateALatticeFromString() {
		//given
		GameOfLife gol = new GameOfLife(staticStateSquare);
		//when
		boolean result=gol.toString().equals(staticStateSquare);
		//then
		assertEquals(true,result);
	}
	
	@Test
	public void shouldNotCreateNonSquareGOL() {
		//given
		boolean exceptionCaught=false;
		//when
		try {
			GameOfLife gol = new GameOfLife(staticStateBad);
		}
		catch(Exception e) {
			exceptionCaught=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void shouldNotUpdateDeadGrid() {
		//given
		boolean exceptionCaught= false;
		GameOfLife gol = new GameOfLife(10);
		//when
		try {
			gol.update();
		}
		catch (Exception e){
			
			exceptionCaught=true;
			System.out.println(e.toString());
		}
		//then
		assertEquals(true,exceptionCaught);
	}
	
	@Test
	public void staticStateSquareShouldNotEvolve() {
		//given
		GameOfLife gol = new GameOfLife(staticStateSquare);
		//when
		gol.update();
		//then
		assertEquals(staticStateSquare,gol.toString());
	}
	
	@Test
	public void staticStateCornerShouldNotEvolve() {
		//given
		GameOfLife gol = new GameOfLife(staticStateCorner);
		//when
		gol.update();
		//then
		assertEquals(staticStateCorner,gol.toString());
	}
	
	@Test
	public void oscillatoryStateBeltHorizontalShouldOscillate() {
		//given
		GameOfLife gol = new GameOfLife(periodicStateBeltHorizontal[0]);
		for(int i=1;i<=100;i++)
		{
			//when
			gol.update();
			//then
			assertEquals(periodicStateBeltHorizontal[i%2],gol.toString());
		}
	}
	
	@Test
	public void oscillatoryStateBeltVerticalShouldOscillate() {
		//given
		GameOfLife gol = new GameOfLife(oscillatoryStateBeltVertical[0]);
		for(int i=1;i<=100;i++)
		{
			//when
			gol.update();
			//then
			assertEquals(oscillatoryStateBeltVertical[i%2],gol.toString());
		}
	}
	
	@Test
	public void unstableStateStringShouldDecay() {
		//given
		GameOfLife gol = new GameOfLife(unstableStateString[0]);
		for(int i=1;i<=2;i++)
		{
			//when
			gol.update();
			//then
			assertEquals(unstableStateString[i],gol.toString());
		}
	}

}
