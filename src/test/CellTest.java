package test;
import static org.junit.Assert.*;

import org.junit.Test;

import GameOfLife.Cell;

public class CellTest {

	@Test
	public void shouldSetupCellDeadDefault() {
		//given
		Cell cell=new Cell();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldSetupLivingCell() {
		//given
		Cell cell=new Cell("1");
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(true,lives);
	}
	
	@Test
	public void shouldSetupDeadCell() {
		//given
		Cell cell=new Cell("0");
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldFlipDeadToAliveCell() {
		//given
		Cell cell=new Cell();
		cell.flip();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(true,lives);
	}
	
	@Test
	public void shouldFlipAliveToDeadCell() {
		//given
		Cell cell=new Cell("1");
		cell.flip();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldSetNumberOfNeighbors() {
		//given
		Cell cell=new Cell();
		cell.setNumberOfNeighbors(2);
		//when
		int neighbors= cell.getNumberOfNeighbors();
		//then
		assertEquals(2,neighbors);
	}
	
	@Test
	public void shouldIncreaseNumberOfNeighbors() {
		//given
		Cell cell=new Cell();
		cell.increaseNeighbors();
		//when
		int neighbors= cell.getNumberOfNeighbors();
		//then
		assertEquals(1,neighbors);
	}
	
	@Test
	public void shouldSurviveWithTwoNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(2);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(true,lives);
	}
	
	@Test
	public void shouldSurviveWithThreeNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(3);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(true,lives);
	}
	
	@Test
	public void shouldComeAliveWithThreeNeighbors() {
		//given
		Cell cell=new Cell("0");
		cell.setNumberOfNeighbors(3);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(true,lives);
	}
	
	@Test
	public void shouldDieWithNoNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(0);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldDieWithOneNeighbor() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(1);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldDieWithFourNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(4);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldDieWithFiveNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(5);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldDieWithSixNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(6);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldDieWithSevenNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(7);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldDieWithEightNeighbors() {
		//given
		Cell cell=new Cell("1");
		cell.setNumberOfNeighbors(8);
		cell.update();
		//when
		boolean lives= cell.isAlive();
		//then
		assertEquals(false,lives);
	}
	
	@Test
	public void shouldResetNeighborsOnUpdate() {
		//given
		Cell cell=new Cell();
		cell.setNumberOfNeighbors(3);
		cell.update();
		//when
		int result= cell.getNumberOfNeighbors();
		//then
		assertEquals(0,result);
	}

}
