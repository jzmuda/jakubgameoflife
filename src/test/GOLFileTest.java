package test;

import static org.junit.Assert.*;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

import GameOfLife.GOLWithFiles;

public class GOLFileTest {

	@Test
	public void shouldSetupFromFile() {
		//given
		GOLWithFiles fileGOL = new GOLWithFiles("sploder.txt",3);
		//when
		boolean correctSetup=fileGOL.getState();
		//then
		assertEquals(true, correctSetup);
	}
	
	@Test
	public void shouldAlertFileNotFound() {
		//given
		String BadFileName="qwerty.txt";
		boolean exceptionCaught=false;
		//when
		try {
			GOLWithFiles fileGOL = new GOLWithFiles(BadFileName,9);
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
			exceptionCaught=true;
		}
		//then
		assertEquals(true, exceptionCaught);
	}
	
	@Test
	public void shouldAlertCorruptFile() {
		//given
		String BadFileName="wtf.txt";
		boolean exceptionCaught=false;
		//when
		try {
			GOLWithFiles fileGOL = new GOLWithFiles(BadFileName,9);
		}
		catch (Exception e)
		{
			System.out.println(e.toString());
			exceptionCaught=true;
		}
		//then
		assertEquals(true, exceptionCaught);
	}

	@Test
	public void shouldAnimate() {
		//given
		GOLWithFiles fileGOL = new GOLWithFiles("sploder.txt",9);
		//when
		fileGOL.animate();
		boolean state=fileGOL.getRound()==9;
		//then
		assertEquals(true, state);
	}
	
	@Test
	public void shouldNotAccessNonExistentRound() {
		//given
		GOLWithFiles fileGOL = new GOLWithFiles("sploder.txt",9);
		boolean exceptionCaught=false;
		//when
		fileGOL.animate();
		try {
			fileGOL.toString(10);
		}
		catch (Exception e) {
			System.out.println(e.toString());
			exceptionCaught=true;
		}
		//then
		assertEquals(true, exceptionCaught);
	}
	
	@Test
	public void shouldSave() {
		//given
		GOLWithFiles fileGOL = new GOLWithFiles("sploder.txt",1);
		Path currentRelativePath = Paths.get("");
		String  FileName= currentRelativePath.toAbsolutePath().toString();
		FileName+="\\src\\GameOfLife\\results\\sploderRound1.txt";
		//when
		fileGOL.animate();
		fileGOL.save();
		File f = new File(FileName);
		//then
		assertEquals(true, f.exists());
	}
	
}
