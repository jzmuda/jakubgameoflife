package GameOfLife;
/**
 * 
 * @author JZMUDA
 *
 *Basic cell object for Game Of Life
 */

public class Cell {
	private boolean alive;
	private int numberOfNeighbors;
	
	public Cell(){
		alive=false;
		setNumberOfNeighbors(0);
	}
	
	public Cell(String state){
		alive=false;
		if(state.equals("1")) alive=true;
		setNumberOfNeighbors(0);
	}
	
	public boolean isAlive() {
		return alive;
	}
	
	public void flip() {
		alive=!alive;
	}

	public int getNumberOfNeighbors() {
		return numberOfNeighbors;
	}

	public void setNumberOfNeighbors(int numberOfNeighbors) {
		this.numberOfNeighbors = numberOfNeighbors;
	}
	
	public void increaseNeighbors() {
		numberOfNeighbors++;
	}
	
	public void update() {
		if ((alive && (numberOfNeighbors==2 || numberOfNeighbors ==3)) || (!alive && numberOfNeighbors==3))
			alive=true;
		else
			alive=false;
		numberOfNeighbors=0;
	}
}
