package GameOfLife;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
/**
 * 
 * @author JZMUDA
 *
 *Simple GOL interface allowing to start the game from file and save the results
 *for each GOL round.
 *Expected input file format: rows of 1's and 0's, equal number of rows and columns
 *Output: text file with name of input and round number.
 *
 */
public class GOLWithFiles {
	private int round;
	private int maxRound;
	private String seed;
	private GameOfLife gol;
	private boolean good;
	private List<String> gameRounds;
	private String fileName;
	
	//Expects input file name and maximum number of rounds
	public GOLWithFiles(String fileName, int maxRound) {
		round=0;
		this.fileName=fileName;
		seed = "";
		this.maxRound=maxRound;
		load();
		try {	
			gol =new GameOfLife(seed);
		}
		catch (Exception e) {
			throw new IllegalArgumentException("Bad File Format or Corrupt File");
		}
		gameRounds= new ArrayList<String>();
		good=true;
	}
	
	private void load() {
		try {
			Path currentRelativePath = Paths.get("");
			String openFileName = currentRelativePath.toAbsolutePath().toString();
			openFileName+="\\src\\GameOfLife\\"+fileName;
			File file = new File(openFileName);
			Scanner input = new Scanner(file);


			while (input.hasNextLine()) {
				String line = input.nextLine();
				seed+=line;
				seed+=" "+GameOfLife.newline;
			}
			input.close();

		} catch (Exception ex) {
			throw new IllegalArgumentException("File not Found");
		}
	}
	
	public boolean getState() {
		return good;
	}
	
	public void animate() {
		while(gol.anyLiving() && (round<maxRound)) {
			gol.update();
			round++;
			gameRounds.add(gol.toString());
		}
		good=false;
	}
	
	public int getRound() {
		return round;
	}
	
	public String toString(int i) {
		if((i<=round)&&(i>0))
			return gameRounds.get(i-1).toString();
		else throw new IllegalArgumentException("Wrong round");
	}
	//Saves all results, separate files for each round
	public void save(){
		for (int i=0;i<gameRounds.size();i++) {
			String front[]=fileName.split("\\.");
			Path currentRelativePath = Paths.get("");
			String  writeFileName= currentRelativePath.toAbsolutePath().toString();
			writeFileName+="\\src\\GameOfLife\\results\\";
			writeFileName+=front[0];
			writeFileName+="Round"+(i+1)+".txt";
			Path file = Paths.get(writeFileName);
			String liness[]= gameRounds.get(i).split(gol.newline);
			List<String> lines = Arrays.asList(liness);
			try {
				Files.write(file, lines, Charset.forName("UTF-8"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}

