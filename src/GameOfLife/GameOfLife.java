package GameOfLife;
/**
 * 
 * 
 * @author JZMUDA
 *
 *
 * Game Of Life on a square Cell lattice with periodic boundary conditions.
 * Alternative implementation possible: two boolean lattices grid and tmpGrid;
 * 
 * 
 */
public class GameOfLife {
	private int gridSize;
	private Cell[][] grid;
	public static final String newline = System.getProperty("line.separator");
	//creates empty Game Of Life grid
	public GameOfLife(int size) {
		if(size<4) throw new IllegalArgumentException("Grid size >=4");
		else {
			gridSize=size;
			grid = new Cell[gridSize][gridSize];
			for(int i=0; i<gridSize; i++)
				for(int j=0; j< gridSize; j++) {
					grid[i][j] = new Cell();
				}
		}
	}
	//imports the initial state from string
	public GameOfLife(String initialState) {
		String rows[] = initialState.split(newline);
		gridSize = rows.length;
		if(gridSize<4) throw new IllegalArgumentException("Grid size >=4");
		else {
			grid = new Cell[gridSize][gridSize];
			for(int i=0; i<gridSize; i++)
			{
				String rowElements[] = rows[i].split(" ");
				if(rowElements.length!=gridSize) throw new IllegalArgumentException("Grid should be square");
				for(int j=0; j< gridSize; j++) {
					grid[i][j] = new Cell(rowElements[j]);
				}
			}
		}
	}
	
	public boolean anyLiving() {
		boolean result=false;
		for(int i=0; i<gridSize; i++)
			for(int j=0; j< gridSize; j++)
				if(grid[i][j].isAlive()) result=true;
		return result;
	}
	//current grid state to string
	public String toString() {
		String result="";
		for(int i=0; i<gridSize; i++) {
			for(int j=0; j< gridSize; j++) {
				if (grid[i][j].isAlive()) result+="1 ";
				else result+="0 ";
			}
			result+=newline;
		}
		return result;
	}
	//changes the (i,j) element state
	public void flip(int i, int j) {
		if(outOfBounds(i, j)) throw new IllegalArgumentException("Grid index out of bounds");
		else {
			grid[i][j].flip();
		}
	}
	
	public boolean isAlive(int i, int j) {
		if(outOfBounds(i, j)) throw new IllegalArgumentException("Grid index out of bounds");
		else {
			return grid[i][j].isAlive();
		}
	}
	
	public int numberOfNeighbors(int i, int j) {
		if(outOfBounds(i, j)) throw new IllegalArgumentException("Grid index out of bounds");
		else {
			return grid[i][j].getNumberOfNeighbors();
		}
	}
	
	private boolean outOfBounds(int i, int j) {
		return (i <0 || j<0 || i> gridSize -1 || j > gridSize -1);
	}
	//performs a single step
	public void update() {
		if(anyLiving())
		{
			countAllNeighbors();
			updateAll();
		}
		else throw new IllegalStateException("Can't update when everyone's dead");
	}
	
	private void countAllNeighbors() {
		for(int i=0; i<gridSize ;i++)
			for(int j=0; j<gridSize; j++)
				countNeighbors(i, j);
	}
	
	private void countNeighbors(int row, int column) {
		if(outOfBounds(row, column)) throw new IllegalArgumentException("Grid index out of bounds");
		else {
			for(int i=-1; i<2 ;i++)
				for(int j=-1; j<2; j++)
				{
					int currentRow=(row+i+gridSize)%gridSize;
					int currentColumn=(column+j+gridSize)%gridSize;
					if (isAlive(currentRow,currentColumn)&&!(currentRow==row && currentColumn==column))
						grid[row][column].increaseNeighbors();
				}
		}
	}
	
	private void updateAll() {
		for(int i=0; i<gridSize ;i++)
			for(int j=0; j<gridSize; j++)
				grid[i][j].update();;
	}
}
